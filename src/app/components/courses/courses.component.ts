import { Component, OnInit } from '@angular/core';
import { CourseService } from  '../services/course.service';;
import { Course } from '../model/course'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  courses: Course[] = [];

  constructor(private courseService: CourseService,private router: Router) {}


  ngOnInit() {
    this.getCourses();
  }

  navigateToDetail(course: Course) {
    let data=JSON.stringify(course)
    this.router.navigate(['/detail', btoa(data)]); // Navigate to the category component with the category name as parameter
  }

  getCourses() {
    this.courseService.getAllCourses()
      .subscribe(
        (data) => {
          this.courses = data;
        },
        (error) => {
          console.error('Error fetching courses:', error);
        }
      );
  }
}
