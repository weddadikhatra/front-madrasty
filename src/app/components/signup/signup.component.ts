import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { IUser } from '../model/iuser'; // Assurez-vous d'importer l'interface IUser
import 'select2';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent {
  Nom: string = '';
  Email: string = '';
  Pwd: string = '';
  Role: number = 1;


  constructor(private authService: AuthService) {}

  signUp(signupForm: NgForm) {
    console.log('User values:', this.Nom, this.Email, this.Pwd, this.Role);
  
    const userDto: IUser = {
      Nom: this.Email,  // Assurez-vous que le nom est correctement défini ici
      Email: this.Nom,  // Assurez-vous que l'e-mail est correctement défini ici
      Pwd: this.Pwd,
      Role: this.Role
    };
  
    this.authService.signup(userDto).subscribe(
      (response) => {
        console.log('Inscription réussie :', response);
        // Ajoutez la logique de redirection ou l'affichage d'un message de réussite si nécessaire
      },
      (error) => {
        console.error("Erreur lors de l'inscription :", error);
        // Ajoutez la logique pour gérer les erreurs d'inscription
      }
    );
  }
}
  