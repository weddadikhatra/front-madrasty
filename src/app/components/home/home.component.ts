import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util.service';
import { CourseService } from '../services/course.service';
import { Course } from '../model/course';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  latestCourses: Course[] | undefined;

  constructor(private util: UtilService, private courseService: CourseService) {}

  ngOnInit(): void {
    this.loadLatestCourses();
  }

  loadLatestCourses() {
    this.courseService.getroisCourses().subscribe(
      (courses: Course[]) => {
        this.latestCourses = courses;
      },
      (error) => {
        console.error('Error loading latest courses:', error);
      }
    );
  }
}
