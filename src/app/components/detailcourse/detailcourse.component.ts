import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from '../model/course';

@Component({
  selector: 'app-detailcourse',
  templateUrl: './detailcourse.component.html',
  styleUrls: ['./detailcourse.component.css']
})
export class DetailcourseComponent implements OnInit {

   course:Course | null = null; 
  
  constructor(private route: ActivatedRoute,private router: Router){}
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
     this.course = JSON.parse(atob(params.get('course')!)) as Course;

        console.log('Course:', this.course.Description);
        // You can now use the category name as needed in this component
      
    }); 
   }

    navigateTopaiment(course: Course) {
      let data=JSON.stringify(course)
      this.router.navigate(['/paiment', btoa(data)]); // Navigate to the category component with the category name as parameter
    }


}
