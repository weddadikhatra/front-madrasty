import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../services/course.service';
import { Course } from '../../model/course';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-mycourses',
  templateUrl: './mycourses.component.html',
  styleUrls: ['./mycourses.component.css']
})


export class MycoursesComponent implements OnInit{
  constructor(
    private CourseService: CourseService,
    private util:UtilService
  ) {}
  mycourses: Course[] = [];
  ngOnInit(): void {
    this.GetMyCourses()
  }
  SignOut(){
    localStorage.removeItem ('Iduser');
    localStorage.removeItem ('Nomuser');
    localStorage.removeItem ('Emailuser');
    this.util.navigateTo('');
  }
  async GetMyCourses(){
    await this.CourseService.GetMycourses().subscribe(
      (st: Course[]) => {
       this.mycourses=st;
       console.log(" st "+this.mycourses[0].Categorie)
      },
      (error) => {
        console.error('Error ', error);
       
      }
    );;
    }
  
    
  
  

}
