import { Component } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { Course } from '../../model/course';
import { CourseService } from '../../services/course.service';
import { Video } from '../../model/Video';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-addcourses',
  templateUrl: './addcourses.component.html',
  styleUrls: ['./addcourses.component.css']
})
export class AddcoursesComponent {

  isSucces = false;
  
   course = {
    title: '',
    description: '',
    category: '',
    price: '',
    imageUrl: '',
    videoUrl: ''
  };

  constructor(private storage: AngularFireStorage,private service:CourseService,private util:UtilService) {}

  onFileSelected(event: any, type: 'image' | 'video') {
    const file = event.target.files[0];
    if (file) {
      const filePath = `${type}/${new Date().getTime()}_${file.name}`;
      const fileRef = this.storage.ref(filePath);
      const task = this.storage.upload(filePath, file);

      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(url => {
            if (type === 'image') {
              this.course.imageUrl = url;
            } else {
              this.course.videoUrl = url;
            }
            console.log("url "+url)
          });
        })
      ).subscribe();
    }
  }

  SignOut(){
    localStorage.removeItem ('Iduser');
    localStorage.removeItem ('Nomuser');
    localStorage.removeItem ('Emailuser');
    this.util.navigateTo('');
  }
  onSubmit() {
    console.log(this.course);
    const courserDto: Course = {
      Description: this.course.description,

      Nom: this.course.title,
      Categorie: this.course.category,
      image:this.course.imageUrl,
      price:Number(this.course.price),
      video:this.course.videoUrl
    };
  
    this.service.Addcourse(courserDto).subscribe(
      (response) => {
       
    
        this.isSucces=true;
        console.log('Inscription réussie :', response);
      },
      (error) => {
        console.error("Erreur lors de l'inscription :", error);
      }
    );
  }
}
