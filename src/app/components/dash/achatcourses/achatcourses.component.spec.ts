import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AchatcoursesComponent } from './achatcourses.component';

describe('AchatcoursesComponent', () => {
  let component: AchatcoursesComponent;
  let fixture: ComponentFixture<AchatcoursesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AchatcoursesComponent]
    });
    fixture = TestBed.createComponent(AchatcoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
