import { Component, OnInit } from '@angular/core';
import { AchatCourse } from '../../model/AchatCourse';
import { CourseService } from '../../services/course.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-achatcourses',
  templateUrl: './achatcourses.component.html',
  styleUrls: ['./achatcourses.component.css']
})
export class AchatcoursesComponent implements OnInit {

  mycourses: AchatCourse[] = [];
  constructor(private service:CourseService,   private util: UtilService,){}

  ngOnInit(): void {
this.GetAchat();
  }
  async GetAchat(){
    await this.service.GetAchat().subscribe(
      (st: AchatCourse[]) => {
       this.mycourses=st;
       console.log("lllll"+this.mycourses.length)
      },
      (error) => {
        console.error('Error ', error);
       
      }
    );
    }

    SignOut(){
      localStorage.removeItem ('Iduser');
      localStorage.removeItem ('Nomuser');
      localStorage.removeItem ('Emailuser');
      this.util.navigateTo('');
    }

}
