import { Component, OnInit } from '@angular/core';
import { DashService } from '../../services/dash.service';
import { Statistique } from '../../model/statistique';
import { Course } from '../../model/course';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  statistique: Statistique = new Statistique(); 
  constructor(
    private DashService: DashService,
   private util:UtilService

    
  ) {}
   nom:string="";
   mycourses: Course[] = [];

   SignOut(){
    localStorage.removeItem ('Iduser');
    localStorage.removeItem ('Nomuser');
    localStorage.removeItem ('Emailuser');
    this.util.navigateTo('');
  }

  ngOnInit(): void {
    this.nom =localStorage.getItem('Nomuser')??"";
    console.log("nooom "+this.nom)
    this.GetStatistique();
    this.GetMyCourses();
    
  }
  async GetStatistique(){
  await this.DashService.GetStatistique().subscribe(
    (st: Statistique) => {
     this.statistique=st;
     console.log(" st "+this.statistique.nbOfCourses)
    },
    (error) => {
      console.error('Error ', error);
     
    }
  );
  }
  async GetMyCourses(){
    await this.DashService.GetMycourses().subscribe(
      (st: Course[]) => {
       this.mycourses=st;
       console.log(" st "+this.mycourses[0].Categorie)
      },
      (error) => {
        console.error('Error ', error);
       
      }
    );
    }
  

}
