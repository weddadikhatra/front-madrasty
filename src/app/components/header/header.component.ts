import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{

  constructor(
    private util: UtilService,
    
  
  ) {}
  ngOnInit(): void {
this.CheckUser() 
 }

  CheckUser(){
    if(localStorage.getItem('Iduser')==null){
      this.util.navigateTo('')
    }
  }
  SignOut(){
    localStorage.removeItem ('Iduser');
    localStorage.removeItem ('Nomuser');
    localStorage.removeItem ('Emailuser');
    this.util.navigateTo('');
  }
}
