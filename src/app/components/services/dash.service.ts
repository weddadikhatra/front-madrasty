import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Statistique } from '../model/statistique';
import { Course } from '../model/course';


@Injectable({
  providedIn: 'root'
})
export class DashService {
  private StatistiQueUrl = 'http://localhost:3000/api/statistique';

  private baseUrl = 'http://localhost:3000/api/courses';

  constructor(private http: HttpClient) {}

  GetStatistique(): Observable<Statistique> {

    return this.http.get<Statistique>(`${this.StatistiQueUrl}`);
  }
  GetMycourses(): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.baseUrl}`);
  }
  
}
