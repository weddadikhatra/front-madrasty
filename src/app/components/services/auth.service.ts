import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser } from '../model/iuser';
import { IAuth } from '../model/iauth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {}

  signup(signupRequest: IUser): Observable<IUser> {
    return this.http.post<IUser>(`${this.baseUrl}/users`, signupRequest);
  }

  login(loginRequest: IAuth): Observable<IUser> {
    return this.http.post<IUser>(`${this.baseUrl}/users/login`, loginRequest);
  }
}
