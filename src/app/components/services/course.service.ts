import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from '../model/course'; 
import { Video } from '../model/Video';
import { AchatCourse } from '../model/AchatCourse';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private baseUrl = 'http://localhost:3000/api/courses';

  constructor(private http: HttpClient) { }

  getAllCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.baseUrl}`);
  }
  getroisCourses(): Observable<Course[]>{
    return this.http.get<Course[]>(`${this.baseUrl}/3lastcourses`);
  }
 
  GetMycourses(): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.baseUrl}`);
  }

  GetAchat(): Observable<AchatCourse[]> {
    return this.http.get<AchatCourse[]>(`http://localhost:3000/api/purchases`);
  }
  
    // Nouvelle méthode pour récupérer les cours par catégorie
    getCoursesByCategory(category: string): Observable<Course[]> {
      return this.http.get<Course[]>(`${this.baseUrl}/category/${category}`);
    }

    Addcourse(addRequest: Course): Observable<Course> {
      return this.http.post<Course>(`${this.baseUrl}`, addRequest);
    }





    

    AddVideo(addRequest: Video): Observable<Video> {
      return this.http.post<Video>(`http://localhost:8090/videos/add`, addRequest);
    }
  }
