import { Component, OnInit } from '@angular/core';
import { Course } from '../model/course';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-paiment',
  templateUrl: './paiment.component.html',
  styleUrls: ['./paiment.component.css']
})
export class PaimentComponent implements OnInit {

  course:Course | null = null; 
 
 constructor(private route: ActivatedRoute,private router: Router){}
 ngOnInit(): void {
   this.route.paramMap.subscribe(params => {
    this.course = JSON.parse(atob(params.get('course')!)) as Course;

       console.log('Course:', this.course.Description);
       // You can now use the category name as needed in this component
     
   }); 
  }

  onSubmit() {
    // Payment processing logic here
    // Navigate on success
    this.router.navigate(['/success']);
  }
  

}
