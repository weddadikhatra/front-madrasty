import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { IUser } from '../model/iuser';
import { IAuth } from '../model/iauth';
import { StateManagerService } from '../../services/state-manager.service';
import { UtilService } from '../../services/util.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  auth: IAuth = { Email:"", Pwd: "" };
  isLoginFailed = false;
  errorMessage: string = '';
   email: string='';
   password: string='';
   AddForm?: FormGroup;

  constructor(
    private util: UtilService,
    private authService: AuthService,
    private stateManagerService: StateManagerService
  ) {}
  ngOnInit(): void {
this.CheckUser()
console.log("uuu "+localStorage.getItem('Iduser'))

  }

  CheckUser(){
    console.log("uuu "+localStorage.getItem('Iduser'))
    if(localStorage.getItem('Iduser')!=null){
      this.util.navigateTo('home')
    }
  }

  login() {
    this.auth.Email=this.email;
    this.auth.Pwd=this.password
    console.log("email "+this.email)

    this.authService.login(this.auth).subscribe(
      (user: IUser) => {
        console.log("useeer "+JSON.stringify(user))
        if (user != null) {
          this.stateManagerService.curUser = user;
          localStorage.setItem ('Iduser',user._id!.toString());
          localStorage.setItem ('Nomuser',user.Nom!.toString());
          localStorage.setItem ('Emailuser',user.Email!.toString());
          if (user.Role === 1) {
            this.util.navigateTo('home');
          } else if (user.Role === 2) {
     
            this.util.navigateTo('dashboard');
  
          }
        } else {
          this.isLoginFailed = true;
          this.errorMessage = 'Invalid email or password';
        }
      },
      (error) => {
        console.error('Error during login:', error);
        this.isLoginFailed = true;
        this.errorMessage = 'An error occurred during login';
      }
    );
  }
}
