export interface IUser {
  _id?:number;
  Nom: string;
  Email: string;
  Pwd: string;
  Role: number;
}
