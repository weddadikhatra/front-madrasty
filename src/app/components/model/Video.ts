import { Time } from "@angular/common";

export interface Video {
    Id?:number;
    Nom: string;
    Crsid: number;
    Time?: Time;
    path: string;
  }
  