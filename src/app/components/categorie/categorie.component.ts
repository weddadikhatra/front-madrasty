import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent {
[x: string]: any;
constructor(private router: Router) { }



navigateToCategory(categoryName: string) {
  const formattedCategory = categoryName.replace(/\s+/g, '-'); // Remplacer les espaces par des tirets
  this.router.navigate(['/CategoCourse', formattedCategory]); // Naviguer vers le composant de catégorie avec le nom de catégorie formaté comme paramètre
}
}
