import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursebycategorieComponent } from './coursebycategorie.component';

describe('CoursebycategorieComponent', () => {
  let component: CoursebycategorieComponent;
  let fixture: ComponentFixture<CoursebycategorieComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoursebycategorieComponent]
    });
    fixture = TestBed.createComponent(CoursebycategorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
