import { Component } from '@angular/core';
import { CourseService } from '../services/course.service';
import { Course } from '../model/course'; 
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-coursebycategorie',
  templateUrl: './coursebycategorie.component.html',
  styleUrls: ['./coursebycategorie.component.css']
})
export class CoursebycategorieComponent {
  courses: Course[] = [];
  categories: string[] = ['Development-Web', 'Web-Designing', 'Developement Mobile', 'Developement Logiciel', 'Marketing', 'Photography', 'Finance & Accounting', 'Inteligence Artificiel'];

  constructor(private route: ActivatedRoute, private courseService: CourseService) {}

  ngOnInit() {
  
    this.route.paramMap.subscribe(params => {
      const categoryName = params.get('category');
      if (categoryName) {
        this.loadCoursesByCategory(categoryName);
        console.log('Category Name:', categoryName);
        // You can now use the category name as needed in this component
      }
    });
  }
  
  loadCoursesByCategory(category: string) {
    this.courseService.getCoursesByCategory(category).subscribe(
      data => {
        this.courses = data;
      },
      error => {
        console.error('Error loading courses by category:', error);
      }
    );
  }
}
