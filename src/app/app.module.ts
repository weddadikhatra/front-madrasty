// app.module.ts

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // Ajoutez cette ligne
import { HttpClientModule } from '@angular/common/http';
import {AngularFireModule} from '@angular/fire/compat';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { DetailcourseComponent } from './components/detailcourse/detailcourse.component';
import { CategorieComponent } from './components/categorie/categorie.component';
import { CoursesComponent } from './components/courses/courses.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { FooterComponent } from './components/footer/footer.component';
import { DashboardComponent } from './components/dash/dashboard/dashboard.component';
import { MycoursesComponent } from './components/dash/mycourses/mycourses.component';
import { AddcoursesComponent } from './components/dash/addcourses/addcourses.component';
import { AchatcoursesComponent } from './components/dash/achatcourses/achatcourses.component';
import { PaimentComponent } from './components/paiment/paiment.component';
import { CoursebycategorieComponent } from './components/coursebycategorie/coursebycategorie.component';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';

import { environment } from '../environments/environment';
import { SuccesComponent } from './succes/succes.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    DetailcourseComponent,
    CategorieComponent,
    CoursesComponent,
    LoginComponent,
    SignupComponent,
    FooterComponent,
    DashboardComponent,
    MycoursesComponent,
    AddcoursesComponent,
    AchatcoursesComponent,
    PaimentComponent,
    CoursebycategorieComponent,
    SuccesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, // Ajoutez cette ligne
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
