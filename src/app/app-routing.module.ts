import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CoursesComponent } from './components/courses/courses.component';
import { CategorieComponent } from './components/categorie/categorie.component';
import { DetailcourseComponent } from './components/detailcourse/detailcourse.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dash/dashboard/dashboard.component';
import { MycoursesComponent } from './components/dash/mycourses/mycourses.component';
import { AddcoursesComponent } from './components/dash/addcourses/addcourses.component';
import { AchatcoursesComponent } from './components/dash/achatcourses/achatcourses.component';
import { PaimentComponent } from './components/paiment/paiment.component';
import { CoursebycategorieComponent } from './components/coursebycategorie/coursebycategorie.component';
import { SuccesComponent } from './succes/succes.component';
const routes: Routes = [
  { path:'', component: LoginComponent },
  { path:'courses', component: CoursesComponent },
  { path:'categories', component: CategorieComponent },
  { path:'detail', component: DetailcourseComponent },
  { path:'signup', component: SignupComponent },
  { path:'home', component: HomeComponent },
  { path:'dashboard', component: DashboardComponent },
  { path:'mycourses', component: MycoursesComponent },
  { path:'addcourses', component: AddcoursesComponent },
  { path:'achatcourses', component: AchatcoursesComponent },
  { path:'paiment/:course', component: PaimentComponent },
  { path: 'CategoCourse/:category', component: CoursebycategorieComponent },
  {path:'detail/:course',component:DetailcourseComponent},
  {path:'success',component:SuccesComponent},
  




 


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
